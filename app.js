const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
var bodyParser = require('body-parser');
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
require('express-async-errors');
const PORT = process.env.PORT || 3000;

const auth = require('./middlewares/auth.mdw');


const app = express();

if(process.env.NODE_ENV !== 'test') {
    //use morgan to log at command line
    app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}

app.use(cors());

app.use(morgan('dev'));
app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));
const options = {
	definition: {
		openapi: "3.0.0",
		info: {
			title: "QLNT",
			version: "1.0.0",
			description: "A simple Express Library API for Quản Lí Nhà Trọ",
		},
		servers: [
			{
				url: `http://localhost:${PORT}`,
			},
		],
	},
	apis: ["./routes/*.js", "./swaggers/*.js"],
};
const specs = swaggerJsDoc(options);

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));

app.use('/api/auth', require('./routes/auth.route'));
app.use('/api/users', require('./routes/user.route'));
app.use('/api/posts',auth,require('./routes/post.route'));

app.get('/', function (req, res) {
    res.json({
        message: "Hello, this is motel management app."
    });
})

app.use(function (req, res, next) {
    res.status(404).json({
        error_massage: 'Endpoint not found'
    })
});

app.use(function (err, req, res, next) {
    console.log(err.stack);
    res.status(500).json({
        error_massage: 'Something error'
    })
});


app.listen(PORT, function () {
    console.log(`Server is running at http://localhost:${PORT}`);
})

module.exports = app;       //for testing



