module.exports = {
    IS_WAITING_APPROVAL: 0,
    IS_APPROVED: 1,
    SECRET_KEY: "SECRET_KEY",
    TIME_OUT_REFRESH_TOKEN:3 * 60,
};