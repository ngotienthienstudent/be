const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const randomsString = require('randomstring');

const userModel = require('../models/user.model');

const validate = require('../middlewares/validate.mdw');
const schema = require('../schemas/login.json');

const router = express.Router();

const constants = require('../constant/status.constant')



router.post('/', validate(schema), async function (req, res) {
  const user = await userModel.findByUserName(req.body.username);
  if (user === null) {
    return res.status(401).json({
      authenticated: false
    });
  }

  if (bcrypt.compareSync(req.body.password, user.password) === false) {
    return res.status(401).json({
      authenticated: false
    });
  }

  const payload = { userId: user.account_id, userName: user.username }
  const opts = {
    expiresIn: constants.TIME_OUT_REFRESH_TOKEN
  }
  const accessToken = jwt.sign(payload, constants.SECRET_KEY, opts);

  const refreshToken = randomsString.generate(80);
  await userModel.patch(user.account_id, {
    rfToken: refreshToken,
  })

  return res.json({
    authenticated: true,
    accessToken,
    refreshToken,
  });
})

router.post('/refreshToken',validate(require("../schemas/refresh.json")), async (req, res)=>{
  const {accessToken,refreshToken} = req.body;
  try {
      const {userId} = jwt.verify(accessToken, constants.SECRET_KEY ,{ignoreExpiration:true});
      const result= await userModel.isValidRefreshToken(userId,refreshToken)
      if(result){
          const payload={
              userId        
          }       
          const opts={
              expiresIn:constants.TIME_OUT_REFRESH_TOKEN
          }
      
          const newAccessToken=jwt.sign(payload,constants.SECRET_KEY ,opts);
          
          return res.json({
              accessToken : newAccessToken
          });           
      }
      return res.status(401).json({
          Message: "Invalid refresh Token"
      })
    } catch(err) {
      return  res.status(401).json({Message: "Invalid accessToken"});
    }
});

module.exports = router;