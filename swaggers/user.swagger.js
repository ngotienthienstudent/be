// Schemas

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       required:
 *         - username
 *         - password
 *         - phone
 *         - role
 *       properties:
 *         username:
 *           type: string
 *           description: username
 *         password:
 *           type: string
 *           description: password
 *         phone:
 *           type: string
 *           description: phone
 *         email:
 *           type: string
 *           description: email
 *         role:
 *           type: string
 *           description: role
 *       example:
 *         username: d5fE_asz
 *         password: 123457
 *         phone: 0943168843
 *         email: adwad@gmail.com
 *         role: 1
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     UserPatch:
 *       type: object
 *       required:
 *         - id
 *         - username
 *         - password
 *         - phone
 *         - role
 *       properties:
 *         id:
 *           type: string
 *           description: id
 *         username:
 *           type: string
 *           description: username
 *         password:
 *           type: string
 *           description: password
 *         phone:
 *           type: string
 *           description: phone
 *         email:
 *           type: string
 *           description: email
 *         role:
 *           type: string
 *           description: role
 *       example:
 *         id: 1
 *         username: d5fE_asz
 *         password: 123457
 *         phone: 0943168843
 *         email: adwad@gmail.com
 *         role: 1
 */

// operate by ID

/**
 * @swagger
 * /api/users/{id}:
 *   get:
 *     summary: Get the user by id
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     responses:
 *       200:
 *         description: The user description by id
 *         contens:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       404:
 *         description: The user was not found
 */

/**
 * @swagger
 * /api/users/{id}:
 *   delete:
 *     summary: Remove the user by id
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 * 
 *     responses:
 *       200:
 *         description: USER DELETED
 *       404:
 *         description: NO USER DELETED
 */


/**
 * @swagger
 * /api/users:
 *   get:
 *     summary: Returns the list of all the users
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: The list of the users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */

/**
 * @swagger
 * /api/users:
 *   post:
 *     summary: Create a new user
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/User'
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /api/users:
 *  patch:
 *    summary: Update the user by the id
 *    tags: [Users]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/UserPatch'
 *    responses:
 *      200:
 *        description: The user was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UserPatch'
 *      404:
 *        description: The user was not found
 *      500:
 *        description: Some error happened
 */







