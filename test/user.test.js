
const app = require('../app');
const assert = require('assert');
const expect = require('chai').expect;
const request = require('supertest');
const supertest = require('supertest');
var randomstring = require("randomstring");


describe('User tests', () => {
    beforeEach((done) => {
        done();
    });
    var token, user, id;
    const un = randomstring.generate({ charset: 'abc', length: 5 });
    describe('User', () => {
        it('should register user', (done) => {
            request(app)
                .post('/api/users')
                .send({
                    "username": un,
                    "password": "123456",
                    "phone": "0123456789",
                    "email": "A@gmail.com",
                    "role": "1"
                })
                .end((err, res) => {
                    user = res.body;
                    assert.strictEqual(res.status, 201);
                    assert.strictEqual(user.username, un);
                    assert.strictEqual(user.email, 'A@gmail.com');
                    assert.strictEqual(user.phone, '0123456789');
                    done();
                })
        });

        it('should new user must be unique', (done) => {
            request(app)
                .post('/api/users')
                .send({
                    "username": un,
                    "password": "123456",
                    "phone": "0123456789",
                    "email": "A@gmail.com",
                    "role": "1"
                })
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    assert.strictEqual(res.status, 200);
                    done();
                })
        });

        it('should user created must have a role', (done) => {
            expect(user.role).to.not.be.undefined;
            expect(user.role).to.be.a('string');
            done();
        });

        it('should logged in user gets a token', function (done) {
            request(app)
                .post('/api/auth')
                .send({
                    username: "test",
                    password: "123"
                })
                .end(function (err, res) {
                    token = res.body.accessToken;
                    assert.strictEqual(res.status, 200);
                    expect(res.body.accessToken).to.not.be.undefined;
                    done();
                });
        });

        it('User can get his/her details', function (done) {
            request(app)
                .get('/api/users/' + user.id)
                .end(function (err, res) {
                    assert.strictEqual(res.status, 200);
                    done();
                });
        });

        it('Authenticated user can see list of all users in the system', (done) => {
            request(app)
                .get('/api/users')
                .set('authorization', token)
                .end((err, res) => {
                    assert.strictEqual(res.status, 200);
                    expect(res.body).to.have.length.above(0);
                    expect(res.body).to.be.instanceof(Array);
                    done();
                });
        });

        it('User can update his/her details', function(done) {
            request(app)
              .patch('/api/users')
              .set('authorization', token)
              .send({
                    "id": user.id,
                    "username": un,
                    "phone": "0123456789",
                    "email": "test@gmail.com",
                    "role": "1"
              })
              .end(function(err, res) {
                assert.strictEqual(res.status, 200);
                expect(user.id).to.not.be.undefined;
                done();
              });
          });

        it('should DELETE user', (done) => {
            request(app)
                .delete('/api/users/' + user.id)
                .set('authorization', token)
                .end((err, res) => {
                    assert.strictEqual(res.status, 200);
                    done();
                });
        });

    })
});
